const slideshow = document.querySelector("section.slideshow")
const images = slideshow.querySelectorAll("img")

slideshow.addEventListener('mousemove', function (event) {
    const x = event.offsetX
    const width = this.offsetWidth

    const percentage = x / width
    const imageNumber = Math.floor(percentage * images.length)

    images.forEach(image => {
        image.style.zIndex = 0
        image.style.opacity = 0
        image.style.transition = "z-index 0.2s step-end, opacity 0.2s linear"
    })

    images[imageNumber].style.zIndex = 1
    images[imageNumber].style.opacity = 1
    images[imageNumber].style.transition = "z-index 0.2s step-start, opacity 0.2s linear"

})